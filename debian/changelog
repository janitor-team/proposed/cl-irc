cl-irc (1:0.9.2+dfsg1-2) unstable; urgency=medium

  * Team upload.
  * Update Vcs-* fields for move to salsa.
  * Set Maintainer to debian-common-lisp@l.d.o.
  * Use secure URL for Homepage.
  * Remove Build-Depends on dh-lisp.
  * Remove obsolete README.building.

 -- Sébastien Villemot <sebastien@debian.org>  Sat, 07 Apr 2018 10:57:16 +0200

cl-irc (1:0.9.2+dfsg1-1) unstable; urgency=medium

  * New upstream version
  * Use caninical Vcs-URLs

 -- Christoph Egger <christoph@debian.org>  Fri, 11 Sep 2015 19:24:57 +0200

cl-irc (1:0.9.1+dfsg1-1) unstable; urgency=low

  [ Luca Capello ]
  * debian/control:
    + remove myself from Uploaders:.

  [ Christoph Egger ]
  * New upstream release
  * Add myself to uploaders
  * New standards version

 -- Christoph Egger <christoph@debian.org>  Sun, 03 May 2015 18:37:49 +0200

cl-irc (1:0.8.1-dfsg-3.1) unstable; urgency=low

  * Non-maintainer upload.
  * Update for flexistream in unstable (Closes: #567812)

 -- Christoph Egger <christoph@debian.org>  Sat, 13 Feb 2010 18:16:20 +0100

cl-irc (1:0.8.1-dfsg-3) unstable; urgency=low

  * debian/control:
    + add Vcs-Browser field.
  * Added debian/README.building file 
  * Now use debhelper v7
  * Updated Standards-Version no real changes

 -- Peter Van Eynde <pvaneynd@debian.org>  Wed, 09 Sep 2009 06:32:40 +0100

cl-irc (1:0.8.1-dfsg-2) unstable; urgency=low

  * debian/changelog:
    + reflect uplodaded versions (Closes: #480242).

  * debian/control:
    + add myself to Uploaders:.
    - remove trailing empty lines.

 -- Luca Capello <luca@pca.it>  Fri, 06 Jun 2008 12:12:30 +0200

cl-irc (1:0.8.1-dfsg-1) unstable; urgency=low

  * Use Vcs-Bzr in control file
  * New upstream version, using again release numbers.
  * Fixed Homepage field
  * Swap binary-arch and binary-indep round.
  * Added usocket dependency
  * updated standard version no real changes
  * Removed dangerous rfc files that eat your children at night.
    (Closes: #443158)
  * Changed to group maintanance
  * Corrected Vcs-Git control field

 -- Peter Van Eynde <pvaneynd@debian.org>  Tue, 19 Feb 2008 06:20:06 +0100

cl-irc (20070905-dfsg-1) unstable; urgency=low

  * Fixed dependency on cl-trivial-sockets (Closes: #440007)
  * New upstream. Major changes: 
    + Make privmsg work for dcc CHAT connections.
    + Make DCC commands and responses work with dcc CHAT connections.
    + Fix incorrect type assumption (reported by antifuchs).

 -- Peter Van Eynde <pvaneynd@debian.org>  Sat, 15 Sep 2007 00:02:20 +0200

cl-irc (20070430-dfsg-1) unstable; urgency=low

  * New upstream version. Major changes:

    + KICK messages generally don't originate at the user being kicked. Fixed.
    + Remove - now unused - .cvsignore file. 
    + No idea why I wrote this, but I think it's generally usefull: hostmask matching.
    + Harmonize keyword expansion in tests/ directory.
    + Fix typo, some refactoring and be more lenient on non-conforming input
    + Silence UNHANDLED messages on the debug stream when they are in fact handled.
    + Fix termination condition in read-protocol-line.
    + Fix off-by-one error and make sure we don't keep looping when the network stream is lost.
    + Move 2 utility routines.
    + Other DCC protocols starting with an #\S have been introduced now (SCHAT,SSEND).
    + Add a list of DCC session types.
    + Rename shadowing local binding (type -> sess-type).
    + Rearrange code. Make dcc-connection an abstract base class.
    + Define fallback functions for the case where the message passed isn't even a subtype of ctcp-mixin.
    + r193 followup: 'user' was renamed to 'remote-user' to disambiguate its meaning.
    + Move connectedp into the dcc-connection section; ignore errors when closing a connection.
    + Create a DCC CHAT message class, just like the IRC message classes we have.
    + r197 followup: disable (yet) unimplemented part (ctcp messages).
    + Add a dcc-chat-connection class; a non-abstract subclass of dcc-connection.
    + r199 followup: read-message calls dcc-message-event. Provide it.
    + Implement CTCP-over-DCC framework.
    + Add missing CTCP primitive: ctcp-reply.
    + Add SSL support for IRC connections, only if CL+SSL is available when calling
    + DCC implementation checkpoint: Working DCC CHAT with passive local side.  'passive local' == either remote initiates or local passive initiative.

 -- Peter Van Eynde <pvaneynd@debian.org>  Mon, 30 Apr 2007 22:54:14 +0200

cl-irc (20070315-dfsg-1) unstable; urgency=low

  * New upstream, fixes minor problem:
    - Fix thinko reported by Lars Rune Nostdal (larsnostdal at gmail dot com).

 -- Peter Van Eynde <pvaneynd@debian.org>  Mon, 09 Apr 2007 01:01:42 +0200

cl-irc (20070106-dfsg-1) experimental; urgency=low

  * Added XS-X-Vcs-Darcs header
  * modified S-X-Vcs-Darcs to XS-Vcs-Darcs field
  * New upstream release. Major changes with many changes.
  * upload to experimental during the freeze

 -- Peter Van Eynde <pvaneynd@debian.org>  Tue,  6 Feb 2007 09:03:53 +0100

cl-irc (20060514-dfsg-1) unstable; urgency=low

  * Removed rfc's, (Closes: #365171)
  * Upstream is in svn, use tailor to convert it to darcs.
  * New upstream release, beyond 0.7.0 now
  * Updated standard version, no real changes
  * Now depends on flexi-streams 

 -- Peter Van Eynde <pvaneynd@debian.org>  Mon, 15 May 2006 02:04:55 +0200

cl-irc (20050321-1) unstable; urgency=low

  * New upstream
  * New versioning scheme, now no longer a native package
  * Updated policy version
  * Now uses darcs-buildpackage

 -- Peter Van Eynde <pvaneynd@debian.org>  Tue,  5 Jul 2005 07:51:23 +0200

cl-irc (0.6.2) unstable; urgency=low

  * Updated package to current CVS (2005-03-03) 

 -- Peter Van Eynde <pvaneynd@debian.org>  Thu,  3 Mar 2005 10:20:59 +0100

cl-irc (0.6.1) unstable; urgency=low

  * New maintainer. (Closes: #297366: O: cl-irc)
  * Adopted by Peter Van Eynde

 -- Peter Van Eynde <pvaneynd@debian.org>  Tue,  1 Mar 2005 10:12:20 +0100

cl-irc (0.6.0) unstable; urgency=low

  * New upstream

 -- Kevin M. Rosenberg <kmr@debian.org>  Thu, 12 Aug 2004 20:47:34 -0600

cl-irc (0.5.2) unstable; urgency=low

  * New upstream

 -- Kevin M. Rosenberg <kmr@debian.org>  Mon, 29 Mar 2004 12:06:43 -0700

cl-irc (0.5.1-1) unstable; urgency=low

  * New upstream
  * Depend on cl-split-sequence

 -- Kevin M. Rosenberg <kmr@debian.org>  Sat, 31 Jan 2004 12:09:59 -0700

cl-irc (0.5-1) unstable; urgency=low

  * New upstream

 -- Kevin M. Rosenberg <kmr@debian.org>  Wed,  7 Jan 2004 05:57:55 -0700

cl-irc (0.0+cvs.2003.12.16-1) unstable; urgency=low

  * Initial upload

 -- Kevin M. Rosenberg <kmr@debian.org>  Tue, 16 Dec 2003 14:28:51 -0700
